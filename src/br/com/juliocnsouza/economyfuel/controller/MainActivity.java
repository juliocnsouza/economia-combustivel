package br.com.juliocnsouza.economyfuel.controller;

import java.sql.SQLException;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.juliocnsouza.economyfuel.db.DatabaseUtil;
import br.com.juliocnsouza.economyfuel.model.Dado;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

public class MainActivity extends Activity {

	private static final String ALCOOL = "Alcool";
	private static final String GASOLINA = "Gasolina";
	private EditText etKmLitroGasolina;
	private EditText etKmLitroAlcool;
	private EditText etValorLitroGasolina;
	private EditText etValorLitroAlcool;
	private TextView tvResposta;
	private Button btResposta;
	private Dado dadoTela;

	private void initcomponents() throws SQLException {
		etKmLitroGasolina = (EditText) findViewById(R.id.etKmLitroGasolina);
		etKmLitroAlcool = (EditText) findViewById(R.id.etKmLitroAlcool);
		etValorLitroAlcool = (EditText) findViewById(R.id.etValorLitroAlcool);
		etValorLitroGasolina = (EditText) findViewById(R.id.etValorLitroGasolina);
		tvResposta = (TextView) findViewById(R.id.tvResposta);
		btResposta = (Button) findViewById(R.id.btResposta);
		dadoTela = getDadoGravado();
		setDadosParaTela();
	}

	private void setDadosParaTela() {
		if (dadoTela.getKmLitroAlcool() != null) {
			etKmLitroAlcool.setText(format(dadoTela.getKmLitroAlcool()));
		}
		if (dadoTela.getKmLitroGasolina() != null) {
			etKmLitroGasolina.setText(format(dadoTela.getKmLitroGasolina()));
		}
		if (dadoTela.getValorLitroAlcool() != null) {
			etValorLitroAlcool.setText(format(dadoTela.getValorLitroAlcool()));
		}
		if (dadoTela.getValorLitroGasolina() != null) {
			etValorLitroGasolina.setText(format(dadoTela.getValorLitroGasolina()));
		}
	}

	private Dado getDadoGravado() throws SQLException {
		dadoTela = getDao().queryForId(1);
		if (dadoTela == null) {
			dadoTela = new Dado();
		}
		return dadoTela;
	}

	private Dao<Dado, Integer> getDao() throws SQLException {
		Dao<Dado, Integer> dao = DaoManager.createDao(new DatabaseUtil(this).getConnectionSource(), Dado.class);
		return dao;
	}

	private void getDadosTela() {
		dadoTela.setKmLitroAlcool(getDouble(etKmLitroAlcool));
		dadoTela.setKmLitroGasolina(getDouble(etKmLitroGasolina));
		dadoTela.setValorLitroAlcool(getDouble(etValorLitroAlcool));
		dadoTela.setValorLitroGasolina(getDouble(etValorLitroGasolina));
	}

	private boolean temZero() {
		return dadoTela.getKmLitroAlcool() == 0.00 || dadoTela.getKmLitroGasolina() == 0.00
				|| dadoTela.getValorLitroAlcool() == 0.00 || dadoTela.getValorLitroGasolina() == 0.00;
	}

	private void Calcular() {
		getDadosTela();
		if (temZero()) {
			tvResposta.setText("Existem valores não preenchidos ou igual a zero!");
		} else {
			double coeficienteGas = dadoTela.getValorLitroGasolina() / dadoTela.getKmLitroGasolina();
			double coeficienteAlc = dadoTela.getValorLitroAlcool() / dadoTela.getKmLitroAlcool();
			String menorCoeficiente = GASOLINA;
			if (coeficienteGas > coeficienteAlc) {
				menorCoeficiente = ALCOOL;
			}
			double porcentagemEconomia = 0.00;
			if (menorCoeficiente.equals(GASOLINA)) {
				porcentagemEconomia = 1 - (coeficienteGas / coeficienteAlc);
			} else {
				porcentagemEconomia = 1 - (coeficienteAlc / coeficienteGas);
			}
			tvResposta.setText("Sua melhor opção é " + menorCoeficiente + "! Economia de "
					+ format((porcentagemEconomia * 100)) + " % em relação ao outro combustivel");
			salvar();
		}
	}

	private String format(Double d) {
		return String.format("%.2f", d).replace(',', '.');
	}

	private void salvar() {
		try {
			getDao().createOrUpdate(dadoTela);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Double getDouble(EditText et) {
		if (et != null && !et.getText().toString().isEmpty()) {
			return Double.parseDouble(et.getText().toString().replace(',', '.'));
		}
		return 0.00;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			initcomponents();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btResposta.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				Calcular();

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
