package br.com.juliocnsouza.economyfuel.db;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import br.com.juliocnsouza.economyfuel.model.Dado;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseUtil extends OrmLiteSqliteOpenHelper {
	private static final String DATABASE_NAME = "combustivel.db";
	private static final int DATABASE_VERSION = 1;

	public DatabaseUtil(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static ConnectionSource getConnectionSource(Context context) {
		return new DatabaseUtil(context).getConnectionSource();
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, Dado.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldDbVersion, int newDbVersion) {
		try {
			TableUtils.dropTable(connectionSource, Dado.class, true);
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
