package br.com.juliocnsouza.economyfuel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Dado {
	@Id
	private Integer id;
	@Column
	private Double kmLitroGasolina;
	@Column
	private Double valorLitroGasolina;
	@Column
	private Double kmLitroAlcool;
	@Column
	private Double valorLitroAlcool;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getKmLitroGasolina() {
		return kmLitroGasolina;
	}

	public void setKmLitroGasolina(Double kmLitroGasolina) {
		this.kmLitroGasolina = kmLitroGasolina;
	}

	public Double getValorLitroGasolina() {
		return valorLitroGasolina;
	}

	public void setValorLitroGasolina(Double valorLitroGasolina) {
		this.valorLitroGasolina = valorLitroGasolina;
	}

	public Double getKmLitroAlcool() {
		return kmLitroAlcool;
	}

	public void setKmLitroAlcool(Double kmLitroAlcool) {
		this.kmLitroAlcool = kmLitroAlcool;
	}

	public Double getValorLitroAlcool() {
		return valorLitroAlcool;
	}

	public void setValorLitroAlcool(Double valorLitroAlcool) {
		this.valorLitroAlcool = valorLitroAlcool;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dado other = (Dado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
